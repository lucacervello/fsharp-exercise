#r "../../packages/FsCheck/lib/net45/FsCheck.dll"

open FsCheck

let rec map f ls =
    match ls with
    | [] -> []
    | x :: xs -> (f x) :: (map f xs)

let l2 = [1 .. 10] |> map (fun (x) -> x * x)

let l3 = [1 .. 10] |> map (fun (x) -> (x, if x % 2 = 0 then "pari" else "dispari"))

let names = [ ("Mario", "Rossi") ; ("Anna Maria", "Verdi") ; ("Giuseppe", "Di Gennaro")]

let names1 = map (fun (name, surname) -> "Dott. " + name + " " + surname) names

let prop_map f (ls : int list) =
    map f ls = List.map f ls

Check.Quick prop_map

let prop_map_pres_len f (ls : int list) =
    List.length (map f ls) = List.length ls

Check.Quick prop_map_pres_len

let rec filter (pred : 'a -> bool) (ls : 'a list) : 'a list =
    match ls with
    | [] -> []
    | x :: xs when pred x -> x :: filter pred xs
    | _ :: xs -> filter pred xs

let mult3 n =
    filter (fun x -> x % 3 = 0) [1 .. n]

let prop_filter pred (ls : int list) =
    filter pred ls = List.filter pred ls

Check.Quick prop_filter

let prop_filter_len pred (ls : int list) =
    List.length(ls) >= List.length(filter pred ls)

Check.Quick prop_filter_len

let filter1 pred ls =
    ((filter pred ls), (filter (fun (x) -> not(pred x)) ls))

let p1 = filter1 (fun x -> x % 3 = 0) [1 .. 20]

let multNonmult n =
    filter1 (fun x -> x % 3 = 0) [1 .. n]

let p2 = multNonmult 16

let prop_filter1_len pred (ls : int list) =
    let (xs , ys) = filter1 pred ls
    List.length(xs @ ys) = List.length(ls)

Check.Quick prop_filter1_len

let prop_filter1_app pred (ls : int list) =
    let (xs, ys) = filter1 pred ls
    List.sort(xs @ ys) = List.sort(ls)

Check.Quick prop_filter1_app

let divisori n =
    filter (fun x -> n % x = 0) [1 .. n]

let d100 =  divisori 100 ;;
// val d100 : int list = [1; 2; 4; 5; 10; 20; 25; 50; 100]

let isPrime n =
    List.length (divisori n) = 2
