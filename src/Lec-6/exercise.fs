#r "../../packages/FsCheck/lib/net45/FsCheck.dll"

(* Capitolo 6 *)

open FsCheck

type figura = 
   | Rettangolo of  int * int      // base * altezza
   | Quadrato   of  int            // lato
   | Triangolo  of  int * int  ;;  // base * altezza

let area fig =
   match fig with
   | Rettangolo(b,h) -> float ( b * h )   
   | Quadrato lato   -> float (lato * lato )
   | Triangolo(b,h)  -> float ( b * h )  / 2.0

let areaOpt fig =
   match fig with
   | Rettangolo (b,h) | Triangolo(b,h)  ->
       if b < 0 || h < 0 then None
       else Some (area fig)
   | Quadrato lato ->
       if lato < 0 then None
       else Some (area fig) 

let sommaArea (fig1, fig2) =
    let areaFig1 = areaOpt fig1
    let areaFig2 = areaOpt fig2
    if areaFig2 = None || areaFig1 = None then
        None
    else
        Some(areaFig1.Value + areaFig2.Value)

let sum1 = sommaArea ( Rettangolo(2,5) , (Quadrato 10) ) ;;
//  val sum1 : float option = Some 110.0

let sum2 = sommaArea ( Rettangolo(2,-5),  (Quadrato 10) ) ;;
// val sum2 : float option = None

let sum3 = sommaArea ( Rettangolo(2, 5), (Quadrato -10) ) ;;
// val sum3 : float option = None

let sum4 =  sommaArea ( Triangolo(10,5), Triangolo(3,5)) ;;
// val sum4  : float option = Some 32.5

let head_tailOpt = function
    | [] -> (None, None)
    | x :: xs -> (Some(x), Some(xs))

let h1 = head_tailOpt [ "uno" ; "due" ; "tre" ] ;;
// val h1 : string option * string list option  =   ( Some "uno", Some ["due"; "tre"] )

let h2 = head_tailOpt ([] : int list) ;;
// val h2 : int option * int list option = (null, null)

let prop_ht_o ls =
    match (head_tailOpt ls) with
    | Some x, Some xs -> ls = (x :: xs)
    | _, None | None, _ -> true

Check.Quick prop_ht_o ;;

let rec lastOpt ls =
    match ls with
    | [] -> None
    | x :: [] -> Some x
    | x :: xs -> lastOpt xs

let l1 = lastOpt [ "uno" ; "due" ; "tre" ] ;;
// val l1 : string option = Some "tre"

let l2 = lastOpt ( [ ] : int list ) ;;
// val l2 : int option = None

let prop_last ls =
    match (lastOpt ls) with
    | Some x -> x = (List.rev ls |> List.head)
    | None -> true

Check.Quick prop_last

let rec catOpt ls =
    match ls with
    | [] -> []
    | Some x :: xs -> x :: catOpt xs
    | None :: xs -> catOpt xs

let lc1 = catOpt ( [Some 1 ; None ; Some 2 ; Some 3 ; None] ) ;;n
// val lc1 : int list = [1; 2; 3]

let lc2 = catOpt ( [ None ; Some "cane" ; None ; None ; Some "gatto" ; Some "topo"] ) ;;
// val lc2 : string list = ["cane"; "gatto"; "topo"]

let prop_cat ls =
    (List.length ls) >= (catOpt ls |> List.length)

Check.Quick prop_cat

let rec mynth (ls, n) =
    match ls, n with
    | [], _ -> None
    | x :: xs, 0 -> Some x
    | x :: xs, _ -> mynth(xs, n - 1)

let y1 = mynth (['a'..'z'], 0) ;;
// val y1 : char option = Some 'a'

let y2 = mynth (['a'..'z'], 2) ;;
// val y2 : char option = Some 'c'

let y3 = mynth (['a'..'z'], 30) ;;
// val y3 : char option = None

let mynth_prop_model (ls, n) =
    match mynth(ls, n) with
    | None -> true
    | Some m -> m = List.nth ls n

Check.Quick mynth_prop_model

let rec mem (x, ls) =
    match ls with
    | [] -> false
    | y :: ys -> x = y || mem(x, ys)

let mynth_prop_mem (ls, n) =
    match mynth(ls, n) with
    | None -> true
    | Some m -> mem(m, ls)

Check.Quick mynth_prop_mem

type tagged = Int of int | Bool of bool

type tList = tagged list

let tl1  = [ Int 0 ] ;;
let tl2  = [ Int 0 ; Int 1 ; Bool true ; Int 4 ; Bool false ] ;; 
let tl3  = [ Int 3 ;  Bool (4>6) ; Int (44-66) ; Bool ( 10 = 5 + 5 )  ];;


let printVal (elem : tagged) =
    match elem with
    | Int e -> String.concat " " [string(e); ": int"]
    | Bool e -> String.concat " " [string(e); ": bool"]

let rec printTl (lst : tList) =
    match lst with
    | x :: [] -> printVal x
    | x :: xs -> String.concat "; " [printVal x; printTl xs]

let s1 = printTl tl1 ;;
// val s1 : string =  "0 : int"

let s2 = printTl tl2 ;;
// val s2 : string = "0 : int; 1 : int; true : bool; 4 : int; false : bool"

let s3 = printTl tl3 ;;
//  val s3 : string =  "3 : int; false : bool; -22 : int; true : bool"

