#r "../../packages/FsCheck/lib/net45/FsCheck.dll"

open FsCheck


type 'a binTree =
    | Null    // empty tree
    | Node of 'a  * 'a binTree * 'a binTree ;;   // Node(root, left, right)


let t2 = Node (2, Null, Node ( 4 , Null, Null ) );; 
let t7 = Node (7, Null, Node (10, Null, Node ( 13 , Null, Null ))) ;; 
let t8 = Node ( 8, Node ( 11, Null, Null), Null ) ;; 
let t5 = Node ( 5, t7, t8 ) ;;
let t9 = Node ( 9, Null,   Node (12, Null, Null) );
let t6 = Node( 6, t9, Null) ;;
let t3 = Node(3, t5, t6 ) ;;
let t1 = Node (1, t2, t3 );;

let rec intToFloatTree (tree : int binTree) : float binTree =
    match tree with
    | Null ->
        Null
    | Node(x, nodeY , nodeZ) ->
        Node (float x, intToFloatTree nodeY, intToFloatTree nodeZ)

let bt7 = intToFloatTree t7 ;;
// val bt7  float E.binTree  = Node (7.0,Null,Node (10.0,Null,Node (13.0,Null,Null)))

let rec inorderToList (tree : 'a binTree) : 'a list =
    match tree with
    | Null -> []
    | Node(value, nodeSx, nodeDx) -> inorderToList(nodeSx) @ [value] @ inorderToList(nodeDx)

let t1inord = inorderToList t1 ;;
// val t1inord : int list = [2; 4; 1; 7; 10; 13; 5; 11; 8; 3; 9; 12; 6]

let rec preorderToList (tree : 'a binTree) : 'a list =
    match tree with
    | Null -> []
    | Node(value, nodeSx, nodeDx) -> preorderToList(nodeSx) @ preorderToList(nodeDx) @ [value]

let prop_visit (btree : int binTree) =
    let l1 = inorderToList btree |> List.sort
    let l2 = preorderToList btree |> List.sort
    l1 = l2

Check.Quick prop_visit

let rec search (key : 'a, btree : 'a binTree) : bool =
    match btree with
    | Null -> false
    | Node ( x, nodeSx, nodeDx) -> x = key || search (key, nodeSx) || search (key, nodeDx)

search(2,t1) ;; // true
search(3,t1) ;; // true
search(4,t1) ;; // true
search(5,t1) ;; // true
search(100,t1) ;; // false

let rec mem (x, ls) =
    match ls with
    | [] -> false
    | y :: ys -> x = y || mem (x, ys)

let prop_search (x, btree : int binTree) =
    let l1 = inorderToList btree
    mem (x, l1) ==> search (x, btree)

Check.Quick prop_search

let rec filterToList (pred : 'a -> bool, btree : 'a binTree) : 'a list =
    match btree with
    | Null ->
        []
    | Node (value, nodeSx, nodeDx) when pred (value) ->
        filterToList (pred, nodeSx) @ [value] @ filterToList (pred, nodeDx)
    | Node (value, nodeSx, nodeDx) ->
        filterToList (pred, nodeSx) @ filterToList (pred, nodeDx)

let isEven x = x % 2 = 0
let isSmall x = x < 5

let t1even = filterToList (isEven,t1 ) ;;
// val t1even : int list = [2; 4; 10; 8; 12; 6]

let t1small = filterToList (isSmall,t1 ) ;;
// val t1small : int list =   [2; 4; 1; 3]

let count (btree) =
    let rec countLeaves = function
        | Null -> 0
        | Node(_, Null, Null) -> 1
        | Node(_, nodeSx, nodeDx) -> countLeaves(nodeSx) + countLeaves(nodeDx)
    let rec countNodes = function
        | Null -> 0
        | Node(_, Null, Null) -> 1
        | Node(_, nodeSx, nodeDx) -> 1 + countNodes(nodeSx) + countNodes(nodeDx)
    (countNodes btree, countLeaves btree)

let n1 = count t2 ;;  // (2, 1)
let n2 = count t7 ;;  // (3, 1)
let n3 = count t6 ;;  // (3, 1)
let n4 = count t1 ;;  // (13, 4)

let rec depthToList (n : int, btree : 'a binTree) : 'a list =
    match btree with
    | Null -> []
    | Node (x, nodeSx, nodeDx) when n = 0 -> [x] @ depthToList (n - 1, nodeSx) @ depthToList (n - 1, nodeDx)
    | Node (x, nodeSx, nodeDx) -> depthToList (n - 1, nodeSx) @ depthToList (n - 1, nodeDx)

let d0 = depthToList (0, t1) ;; // [1]  
let d1 = depthToList (1, t1) ;; // [2; 3]
let d2 = depthToList (2, t1) ;; // [4; 5; 6]
let d3 = depthToList (3, t1) ;; // [7; 8; 9]
let d4 = depthToList (4, t1) ;; // [10; 11; 12]
let d5 = depthToList (5, t1) ;; // [13]
let d7 = depthToList (100, t1) ;; // []

type direction = L | R

let rec getElement (path : direction list, btree : 'a binTree) : 'a option =
    match path, btree with
    | [], Null -> None
    | _, Null -> None
    | [], Node (x, _, _) -> Some x
    | L :: xs, Node (x, nodeSx, _) -> getElement (xs, nodeSx)
    | R :: xs, Node (x, _, nodeDx) -> getElement (xs, nodeDx)

let g1 = getElement ( [], t1 ) ;;  //  Some 1
let g2 = getElement ( [L], t1 ) ;; //  Some 2
let g3 = getElement ( [L ; L], t1 ) ;; // None
let g4 = getElement ( [L ; R], t1 ) ;; // Some 4
let g5 = getElement ( [L ; R ; R], t1 ) ;; // None 
let g6 = getElement ( [R ; L ; L], t1 ) ;; // Some 7
let g7 = getElement ( [R ; L ; L ; L ; L], t1 ) ;; // None
let g8 = getElement ( [R ; L ; L ; R ; R], t1 ) ;; // Some 13 

// i dont like how it looks the pattern matching
let rec insert (value : 'a, btree : 'a binTree) : 'a binTree =
    match btree with
    | Null ->
        Node (value, Null, Null)
    | Node (x, nodeSx, nodeDx) when x < value ->
        Node (x, nodeSx, insert (value, nodeDx))
    | Node (x, nodeSx, nodeDx) ->
        Node (x, insert (value, nodeSx), nodeDx)

let prop_insert (x : int, btree : int binTree) =
    search (x, insert (x, btree))

Check.Quick prop_insert

let rec insertFromList (ls : 'a list, btree : 'a binTree) : 'a binTree =
    match ls with
    | [] -> btree
    | x :: xs -> insertFromList (xs, insert (x, btree))

let prop_insertFromList (ls : int list, btree : int binTree) =
    List.forall (fun x -> prop_insert (x, btree)) ls

Check.Quick prop_insertFromList

let intList = [ 20 ; 10 ; 60 ; 15 ; 40 ; 100 ; 30 ; 50 ; 70 ; 35 ; 42 ; 58 ; 75 ; 32 ; 37 ] ;;
let strList1 = [ "pesca" ; "banana" ; "uva" ; "albicocca" ; "nocciola" ; "ribes" ] ;;
let strList2 = [ "limone" ; "ciliegia" ; "mela" ; "pera" ; "noce"  ] ;;

let intTree = insertFromList (intList, Null)
let strTree1 = insertFromList (strList1, Null)
let strTree2 = insertFromList (strList2, Null)

let rec search1 (key : 'a, btree : 'a binTree) : bool =
    match btree with
    | Null -> false
    | Node (x, _, _) when x = key -> true
    | Node (x, Node (y, _, _), nodeSx) when key > y -> search1 (key, nodeSx)
    | Node (x, nodeDx, _) -> search1 (key, nodeDx)

let prop_search1 (key : int, btree : int binTree) : bool =
    search (key, btree) = search1 (key, btree)

Check.Quick prop_search1

let searchPath (key : 'a, btree : 'a binTree) : 'a list =
    let rec searchPathIntern (key : 'a, btree : 'a binTree) : 'a list =
        match btree with
        | Null -> []
        | Node (x, _, _) when x = key -> [x]
        | Node (x, _, nodeSx) when key > x -> x :: searchPath (key, nodeSx)
        | Node (x, nodeDx, _) -> x :: searchPath (key, nodeDx)
    let result = searchPathIntern (key, btree)
    if mem (key, result) then
        result
    else
        []

let p1 = searchPath (10, intTree) ;; // [20; 10]
let p2 = searchPath (20, intTree) ;; // [20]
let p3 = searchPath (40, intTree) ;; // [20; 60; 40]
let p4 = searchPath (32, intTree) ;; // [20; 60; 40; 30; 35; 32]
let p5 = searchPath (11, intTree) ;; // []

let rec min (btree : 'a binTree) : 'a option =
    match btree with
    | Null -> None
    | Node (x, Null, _) -> Some x
    | Node (_, nodeSx, _) -> min (nodeSx)

min intTree ;;   //   Some 10
min strTree1;;   //  Some "albicocca"
min ( Null : int binTree) ;; // None

let rec subtree (key : 'a, btree : 'a binTree) : 'a binTree =
    match btree with
    | Null -> Null
    | Node (x, nodeSx, nodeDx) when x = key -> Node (x, nodeSx, nodeDx)
    | Node (x, _, nodeSx) when key > x -> subtree (key, nodeSx)
    | Node (x, nodeDx, _) -> subtree (key, nodeDx)

let m1 = min ( subtree(10, intTree) )  ;;    // Some 10
let m2 = min ( subtree(15, intTree) )  ;;    // Some 15
let m3 = min ( subtree(60, intTree) )  ;;    // Some 30
let m4 = min ( subtree(40, intTree) ) ;;     // Some 30
let m5 = min ( subtree(100, intTree) ) ;;    // Some 70
let m6 = min ( subtree(1000, intTree) ) ;;   // None
let m7 = min ( ( subtree ("limone",  strTree2) ) ) ;;  //  Some "ciliegia"
let m8 = min ( ( subtree ("ribes",  strTree1) ) )  ;;  // Some "ribes"
