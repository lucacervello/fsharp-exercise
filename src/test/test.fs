#r "../../packages/FsCheck.NUnit/lib/net45/FsCheck.NUnit.dll"
#r "../../packages/FsCheck/lib/net45/FsCheck.dll"

open FsCheck
open FsCheck.NUnit

[<Test>]
let ``When 2 is added to 2 expect 4``() = 
    Assert.AreEqual(4, 2+2)
